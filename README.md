# SIDEKICK

This is SIDEKICK (SupervIseD Expert Knowledge Influenced Correlation Clustering), 
a first semi-supervised correlation clustering algorithm.
This repo includes the code and the dataset used for the evaluation in the paper.
