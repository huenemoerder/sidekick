import pandas as pd
import seaborn as sb
import matplotlib.pyplot as plot

data = pd.read_csv('3D_correlation_clusters.csv')



data_without_outliers = data.loc[data['Cluster'] >= 0].copy()
data_with_outliers = data.copy()

for index, point in data.iterrows():
    data.loc[index, "Cluster"] = 'C' + str(point["Cluster"])

for index, point in data_without_outliers.iterrows():
    data_without_outliers.loc[index, "Cluster"] = 'C' + str(point["Cluster"])


sb.pairplot(data_without_outliers, hue='Cluster')
sb.pairplot(data, hue="Cluster")
plot.show()