from functions import derive_model, predict, distance_point_hyperplane, calculateStandardDeviation
import numpy as np
import pandas as pd

def constrained_correlation_clustering(data, number_of_clusters, alpha, version=0, k=0, deviations=[]):
    # The data is split into ground truth and unlabeled points, the unlabeled points are put together with the noise
    # as "Cluster -1", the ground truth instances all keep their label, the actual labeling is kept in the first part
    # of a combined key
    #data = make_ground_truth(data, ratio=0.05)

    models = []
    distances_cluster = [[], [], []]
    distances_noise = [[], [], []]

    # A model is calculated for each cluster / ground truth and appended to models
    for c in range(0, number_of_clusters):
        cluster = data.loc[c, :]
        gt = cluster[cluster["Cluster"] == c].drop("Cluster", axis=1)
        model = derive_model(gt, c, alpha)
        models.append(model)

    # the rest of the data is the unlabeled data, though it might include noise
    unlabeled = data[data["Cluster"] <= -1].drop(["Cluster"], axis=1).sample(frac=1)

    for index, point in unlabeled.iterrows():
        prediction = predict(models, point)
        actual_label = index[0]
        point_index = index[1]
        model = models[prediction]
        data.loc[(actual_label, point_index), 'Actual'] = actual_label
        distances = []

        if version == 0 or version == 5:
            # Just label all points that are predicted to be part of a cluster to the corresponding cluster
            data.loc[(actual_label, point_index), 'Cluster'] = prediction
            if version == 5:
                data.loc[(actual_label, point_index), 'Distance'] = distance_point_hyperplane(model.strong, model.mean, point)
        elif version == 1 or version == 4:
            # Only label points that are in a k-sigma neighbourhood of the ground truth hyperplane
            distance = distance_point_hyperplane(model.strong, model.mean, point)
            if distance < (k * model.sd):
                data.loc[(index[0], index[1]), 'Cluster'] = prediction
                #distances_cluster[prediction].append(distance)
            #else:
                #distances_noise[prediction].append(distance)
        elif version == 3:
            # Only label points that are in a alpha correlated neighbourhood of the ground truth hyperplane
            distance = distance_point_hyperplane(model.strong, model.mean, point)
            if distance <= 3*(np.sqrt((1/deviations[model.id]-1) * np.sum(model.strong_values))):
                data.loc[(index[0], index[1]), 'Cluster'] = prediction
                #if model.id == 0:
                   #distances_cluster[prediction].append(distance)
            #else:
                #distances_noise[prediction].append(distance)

    updated_models = []

    #recalculate the models for each resulting clustering for comparison
    for model in models:
        if version == 5:
            updated_model = derive_model(data.loc[data["Cluster"] == model.id].drop(['Cluster', 'Actual', 'Distance'], axis=1),
                                        model.id + 10, alpha)
        else:
            updated_model = derive_model(
                data.loc[data["Cluster"] == model.id].drop(['Cluster', 'Actual'], axis=1),model.id + 10, alpha)
        updated_models.append(updated_model)
        #print("Ground Truth:" + str(model))
        #print("Updated:" + str(updated_model))
        #print()


    if version == 4:
        distances = []
        for index, point in data[data["Cluster"] <= -1].drop(["Cluster", "Actual"], axis=1).iterrows():
            prediction = predict(updated_models, point)
            updated_model = updated_models[prediction]
            distance = (distance_point_hyperplane(updated_model.strong, updated_model.mean, point))

            if distance < 3 * np.sqrt((1-alpha) * np.sum(updated_model.values)):
                data.loc[index, "Cluster"] = updated_model.id-10

        for updated_model in updated_models:
            final_model = derive_model(data.loc[data["Cluster"] == updated_model.id-10].drop(['Cluster', 'Actual'], axis=1),
                                         updated_model.id + 10, alpha)
            #print("Even more Updated:" + str(final_model))
            # print()


    gaussians = []
    if version == 5:
        for model in models:
            cluster = data.loc[data['Cluster'] == model.id].sort_values(by=['Distance'])
            i = 0
            for index, point in cluster.iterrows():
                if not np.isnan(point['Distance']):
                    distances.append(point['Distance'])
                    term1 = 1/(model.sd * np.sqrt(2 * np.pi))
                    term2 = np.power(np.e, (-1 * (point['Distance'] ** 2)) / (2 * (model.sd**2)))
                    #print(np.var(distances))
                    gaussians.append(term1 * term2)
                    if np.std(distances) > np.sqrt((1-alpha) * np.sum(model.values)) and not np.isnan(point['Distance']):
                        data.loc[index, "Cluster"] = -1
                            #print(point)
        print(np.square(model.sd))


    confusion = pd.DataFrame(np.zeros((number_of_clusters + 1, number_of_clusters + 1)))

    for _, point in data.iterrows():
        if not np.isnan(point["Actual"]):
            actual_label = int(point["Actual"])
            prediction = int(point['Cluster'])
            if actual_label >= 0 and prediction >= 0:
                confusion.iloc[actual_label, prediction] = confusion.iloc[actual_label, prediction] + 1
            elif prediction < 0:
                confusion.iloc[actual_label, number_of_clusters] = confusion.iloc[actual_label, number_of_clusters] + 1
            elif actual_label < 0:
                confusion.iloc[number_of_clusters, prediction] = confusion.iloc[number_of_clusters, prediction] + 1
            else:
                confusion.iloc[number_of_clusters, number_of_clusters] = confusion.iloc[number_of_clusters, number_of_clusters] + 1


    return confusion, data



