import numpy as np
from sklearn.decomposition import PCA
import pandas as pd
from scipy.linalg import solve

#from sympy import Matrix, Symbol, linsolve


class Model:
    '''
        Each cluster is described by a model containing all values needed for
        the necessary calculations.
        The strong and weak eigenvectors are used to describe the hyperplane
        Strong for distance calculations and weak for explainability
        The standart deviation is used to classify additional points
    '''
    def __init__(self, id, strong, weak, values, dim, mean, sd):
        self.id = id
        self.strong = strong
        self.weak = weak
        self.values = values
        self.dim = dim
        self.strong_values = values[:dim]
        self.mean = mean
        self.sd = sd
        if (self.dim == len(self.strong) + len(self.weak)):
            self.correlated = False
            self.dim = self.dim - 1
            self.weak = self.strong[-1]
            self.strong = self.strong[:-1]
            print('WARNING :: Cluster ' + str(id) + " is not correlated")
        else:
            self.correlated = True

    def __str__(self):
        return "Cluster " + str(self.id) + ": strong=" + str(self.strong) \
               + ", weak=" + str(self.weak) + ", values= " + str(self.values) + ", values= " + str(np.sum(self.strong_values)/np.sum(self.values)) + ", dim=" + str(self.dim) + \
               ", mean=" + str(self.mean) + ", std=" + str(self.sd)
    
def derive_model(cluster, index, alpha):
    '''
    :param cluster:
    :param index:
    :param alpha:
    :return model, point:

    All necessary values are caluclated
    First we find the strong and weak principal components
    using scipy's PCA
    Then we calculate the distance of each point contained in the cluster
    '''
    alpha = 0.90
    pca = PCA(random_state=42)
    pca.fit(cluster)
    strong, weak, dim = select_weak_eigenvectors(pca.components_, pca.explained_variance_ratio_, alpha)
    model = Model(index, strong, weak, pca.explained_variance_, dim, pca.mean_, 0)
    model.sd, _ = calculateStandardDeviation(cluster, model)
    return model

#Already sorted cause sklearn pca
def select_weak_eigenvectors(vectors, strength_values, alpha):
    '''
    Since scipy's PCA returns the variance ratio

    :param vectors: eigenvectors
    :param strength_values: explained variance ratio
    :param alpha:
    :return strong and weak eigenvectors and correlation dimensionality:
    '''
    dim = 1
    strength = 0
    for value in strength_values:
        strength = strength + value
        if strength >= alpha:
            break
        dim = dim + 1
    #if dim == len(vectors):
    #    dim = dim - 1
    strong = vectors[:dim]
    weak = vectors[dim:]
    return strong, weak, dim


def distance_point_hyperplane(strong, mean, point):
    '''

    :param strong:
    :param mean:
    :param point:
    :return:
    '''
    point = point - mean
    projection = point
    for s in strong:
        v = np.dot(s, point)
        projection = projection - (v * s)

    return np.round(np.linalg.norm(projection), decimals=12)

def calculateStandardDeviation(cluster, model):
    distances = []
    for index, point in cluster.iterrows():
        distance = distance_point_hyperplane(model.strong, model.mean, point=point)
        distances.append(distance)
    return np.std(distances), distances

def predict(models, point):
    '''

    :param models:
    :param point:
    :return:
    '''
    probabilites = []
    gaussian_values = []

    for model in models:
        distance = distance_point_hyperplane(model.strong, model.mean, point)
        if (model.sd == 0):
            model.sd = 0.0000001

        term1 = 1 / (model.sd * np.sqrt(2 * np.pi))
        term2 = np.power(np.e, (-1 * (distance ** 2)) / (2 * (model.sd ** 2)))
        if (term1 * term2) < 1.0e-143:
            gaussian_values.append(1.0e-143)
        else:
            gaussian_values.append(term1 * term2)

        #print('For Model ' + str(model.id) + ': Distance: ' + str(distance) + ', P: ' + str(term1*term2))

    gauss_total = np.sum(gaussian_values)
    for gaussian in gaussian_values:
        probabilites.append(gaussian/gauss_total)

    #print(probabilites)
    if np.max(probabilites) >= 0:
        return probabilites.index(np.max(probabilites))
    else:
        return -1

def simplifyAndDisplay(weak, mean, features):
    '''

    :param weak:
    :param mean:
    :param features:
    :return:
    '''
   # print(weak)
    b = np.zeros(2)
    print(solve(weak, b))
    print(features)
    return

# Randomly choose a percentage of points from each cluster, these are labeled as belonging to the corresponding cluster,
# Every other point is labeled as -1
def make_ground_truth(df, ratio=0.90, seed=0):
    temp = []
    keys = []
    for c in df["Cluster"].unique():
        keys.append(c)
        cluster = df[df["Cluster"] == c].copy()
        if c <= -1:
            temp.append(cluster)
        else:
            length, _ = cluster.shape
            rest = cluster.sample(random_state=seed, frac=ratio)
            #print(rest.shape)
            rest.Cluster = -1
            cluster.update(rest)
            temp.append(cluster)
    result = pd.concat(temp, keys=keys)

    for c in result["Cluster"].unique():
        if c > -1:
            cluster = result[result["Cluster"] == c]
        cluster = result[result["Cluster"] == c]

    return result
